//
//  Test_ProductsService.swift
//  ZircleTests
//
//  Created by Mohammad Zakizadeh on 1/11/21.
//

import XCTest
@testable import Zircle

final class ProductsServiceTests: XCTestCase {
    
    var sut: ProductService?
    var productsJson: Data?
    var categoriesJson: Data?
    
    override func setUp() {
        let bundle = Bundle(for: type(of: self))
        if let path = bundle.path(forResource: "products", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                self.productsJson = data
            } catch {
                
            }
        }
        
        if let path = bundle.path(forResource: "categories", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                self.categoriesJson = data
            } catch {
                
            }
        }
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func test_getProducts() {
        
        // Given
        let urlSessionMock = URLSessionMock()
        urlSessionMock.data = productsJson
        let mockRequestManager = RequestManagerMock(session: urlSessionMock, validator: MockResponseValidator())
        sut = ProductService(requestManager: mockRequestManager)
        let expectation = XCTestExpectation(description: "Async products test")
        var products: Products?
        
        // When
        sut?.getProducts(page: 0, completionHandler: { (result) in
            defer {
                expectation.fulfill()
            }
            switch result {
            case .success(let catalog):
                products = catalog
            case .failure:
                XCTFail()
            }
        })
        
        // Then
        wait(for: [expectation], timeout: 5)
        XCTAssertTrue(products?.total == 123)
    }
    
    func test_getCategories() {
        
        // Given
        let urlSessionMock = URLSessionMock()
        urlSessionMock.data = categoriesJson
        let mockRequestManager = RequestManagerMock(session: urlSessionMock, validator: MockResponseValidator())
        sut = ProductService(requestManager: mockRequestManager)
        let expectation = XCTestExpectation(description: "Async categories test")
        var categories: ProductsCategories?
        
        // When
        sut?.getProductsCategories(completionHandler: { (result) in
            defer {
                expectation.fulfill()
            }
            switch result {
            case .success(let catalog):
                categories = catalog
            case .failure:
                XCTFail()
            }
        })
        
        // Then
        wait(for: [expectation], timeout: 5)
        XCTAssertTrue(categories?.categories?.count == 5)
        
    }
}


