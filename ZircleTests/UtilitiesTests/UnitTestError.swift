//
//  UnitTestError.swift
//  ZircleTests
//
//  Created by Mohammad Zakizadeh on 1/11/21.
//

import Foundation

struct UnitTestError: Error {}
