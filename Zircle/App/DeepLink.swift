//
//  DeepLink.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/9/21.
//

import Foundation

// not used in app, but is an enum for controlling and navigating with deeplinks

enum DeepLink {
    case category(name: String)
}
