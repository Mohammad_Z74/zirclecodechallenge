//
//  ProductsCategories.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import Foundation

struct ProductsCategories: Codable {
    var categories: [String]?
}
