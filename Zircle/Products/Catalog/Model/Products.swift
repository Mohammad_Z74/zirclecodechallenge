//
//  Products.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import Foundation

// MARK: - Products
struct Products: Codable {
    let result: [Product]?
    let next: String?
    let prev: String?
    let total: Int?
}

// MARK: - Product
struct Product: Codable {
    let itemID: String?
    let image: String?
    let title: String?
    let price: Price?
    let resultDescription: String?
    let category: String?

    enum CodingKeys: String, CodingKey {
        case itemID = "item_id"
        case image, title, price
        case resultDescription = "description"
        case category
    }
}

// MARK: - Price
struct Price: Codable {
    let value: Double?
    let currency: Currency?
}

enum Currency: String, Codable {
    case currency = "€"
    case empty = "$"
    case purple = "£"
}
