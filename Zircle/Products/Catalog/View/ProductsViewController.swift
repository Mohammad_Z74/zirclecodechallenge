//
//  ProductsViewController.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/9/21.
//

import UIKit

class ProductsViewController: UIViewController, Storyboarded {
    
    // MARK: - Properties
    @IBOutlet var categoriesCollectionView: UICollectionView!
    @IBOutlet var productsTableView: UITableView!
    
    var productsTableViewDataSource: ZircleTableViewDataSource<ProductTableViewCell>!
    var categoriesCollectionViewDataSource: ZircleCollectionViewDataSource<CategoryCell>!
        
    weak var coordinator: ProductCoordinator?
    
    let productsViewModel = ProductsViewModel(productsService: ProductService.shared)
    
    // MARK: - ViewCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupBindings()
        getData()
    }
    
    // MARK: - Customizing View
    private func setupView() {
        // Navigation Controller
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        if let layout = categoriesCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
            layout.scrollDirection = .horizontal
            layout.minimumInteritemSpacing = 40
            layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
        // categoriesCollectionView
        categoriesCollectionViewDataSource = ZircleCollectionViewDataSource(items: [], collectionView: categoriesCollectionView, delegate: self)
        categoriesCollectionView.delegate = categoriesCollectionViewDataSource
        categoriesCollectionView.dataSource = categoriesCollectionViewDataSource
        categoriesCollectionView.showsHorizontalScrollIndicator = false
        
        // productsTableViewDataSource
        productsTableViewDataSource = ZircleTableViewDataSource(cellHeight: nil, items: [], tableView: productsTableView, delegate: self, animationType: .type1(0.5))
        productsTableView.delegate = productsTableViewDataSource
        productsTableView.dataSource = productsTableViewDataSource
    }
    
    // MARK: - Bindings
    
    private func setupBindings() {
        
        // Subscribe to Loading
        productsViewModel.loading = { [weak self] isLoading in
            guard let self = self else { return }
            isLoading ? self.view.animateActivityIndicator() : self.view.removeActivityIndicator()
        }
        
        // Subscribe to categories
        productsViewModel.categories = { [weak self] categories in
            guard let self = self else { return }
            // Show Categories to collectionView
            self.categoriesCollectionViewDataSource.appendItemsToCollectionView(categories)
        }
        
        // Subscribe to Products
        productsViewModel.products = { [weak self] products in
            guard let self = self else { return }
            // Add new products to tableView dataSource.
            self.productsTableViewDataSource.appendItemsToTableView(products)
        }
        
        // Subscribe to errors
        productsViewModel.errorHandler = { [weak self] error in
            guard let self = self else { return }
            let ac = UIAlertController(title: "Error",
                                        message: error,
                                        preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(ac, animated: true, completion: nil)
        }
        
    }
    
    private func getData() {
        productsViewModel.getCatalog()
    }
}

// MARK: - ZircleCollectionViewDelegate
extension ProductsViewController: ZircleCollectionViewDelegate {
    func collection(willDisplay cellIndexPath: IndexPath, cell: UICollectionViewCell) {
        guard let categoryCell = cell as? CategoryCell else { return }
        let isAnAppliedFilter = productsViewModel.appliedFilters.contains(categoryCell.category)
        categoryCell.isCategorySelected = isAnAppliedFilter
        
    }
    
    func collection(_ collectionView: UICollectionView, didSelectItem index: IndexPath) {
        guard let categoryCell = collectionView.cellForItem(at: index) as? CategoryCell else { return }
        // Change isSelected state of cell
        categoryCell.isCategorySelected = !(categoryCell.isCategorySelected)
        // add or remove filter depending of categoryCell state
        categoryCell.isCategorySelected ? productsViewModel.add(categoryToFilter: categoryCell.category)
            : productsViewModel.remove(categoryFromFilter: categoryCell.category)
        // Get new filtered products from view model
        let filteredProducts = productsViewModel.applyFilters(filters: productsViewModel.appliedFilters)
        // Refresh Products tableView with new filtered array
        self.productsTableViewDataSource.refreshWithNewItems(filteredProducts)

    }
}
// MARK: - ZircleTableViewDelegate

extension ProductsViewController: ZircleTableViewDelegate {
    func tableView(willDisplay cellIndexPath: IndexPath, cell: UITableViewCell) {
        // For pagination
        if cellIndexPath.row == (productsTableViewDataSource.items.count - 1) && !productsViewModel.isFinished {
            productsViewModel.getProducts()
        }
    }
}
