//
//  ProductTableViewCell.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var productTitle: UILabel!
    @IBOutlet var productSubtitle: UILabel!
    @IBOutlet var productPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        // productImageView
        productImageView.contentMode = .scaleToFill
        
        // productTitle
        productTitle.font = UIFont.boldSystemFont(ofSize: 14)
        productTitle.textAlignment = LanguageManager.shared.currentLanguage.textAlignment
        
        // productSubtitle
        productSubtitle.font = UIFont.systemFont(ofSize: 14, weight: .light)
        productSubtitle.textAlignment = LanguageManager.shared.currentLanguage.textAlignment
        productSubtitle.numberOfLines = 3
        
        // productPrice
        productPrice.font = UIFont.boldSystemFont(ofSize: 14)
        productPrice.textAlignment = LanguageManager.shared.currentLanguage.textAlignment
    }
    
    override func prepareForReuse() {
        self.productImageView.image = nil
    }
}

extension ProductTableViewCell: ZircleTableViewCell {
    func configureCellWith(_ item: Product) {
        if let url = URL(string: item.image ?? "") {
            productImageView.load(url: url)
        }
        productTitle.text = item.title ?? ""
        productSubtitle.text = item.resultDescription ?? ""
        
        productPrice.text = "\(item.price?.currency?.rawValue ?? "") \(item.price?.value ?? 0)"
    }
}
