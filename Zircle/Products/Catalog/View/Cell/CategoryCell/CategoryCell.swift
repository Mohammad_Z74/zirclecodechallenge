//
//  CategoryCell.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var containerView: UIView!
    
    var isCategorySelected: Bool = false {
        didSet {
            isSelected(selected: isCategorySelected)
        }
    }
    
    var category: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView() {
        
        containerView.layer.cornerRadius = 15
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor.zalandoOrange.cgColor

        categoryLabel.textAlignment = .center
        categoryLabel.font = UIFont.boldSystemFont(ofSize: 14)
        
    }
    
    private func isSelected(selected: Bool) {
        if selected {
            containerView.backgroundColor = .zalandoOrange
            categoryLabel.textColor = .white
            
        } else {
            containerView.backgroundColor = .white
            categoryLabel.textColor = .black
        }
    }
}

extension CategoryCell: ZircleCollectionViewCell {
    func configureCellWith(_ item: String) {
        self.category = item
        categoryLabel.text = item
    }
}
