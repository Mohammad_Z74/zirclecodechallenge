//
//  ProductsService.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import Foundation

/*

 This is Product Service, responsible for making api calls of getting products.
 
 */

typealias CategoriesCompletionHandler = (Result<ProductsCategories, RequestError>) -> Void
typealias ProductsCompletionHandler = (Result<Products, RequestError>) -> Void

protocol ProductsServiceProtocol {
    func getProductsCategories(completionHandler: @escaping CategoriesCompletionHandler)
    func getProducts(page: Int, completionHandler: @escaping ProductsCompletionHandler)
}

/*
 ProductsEndpoint is URLPath of Product Api calls
 */

private enum ProductsEndpoint {
    case categories
    case products(Int)
    
    var path: String {
        switch self {
        case .categories:
            return "categories"
        case .products(let page):
            if page < 1 {return "catalog"}
            return "catalog\(page)"
        }
    }
}

class ProductService: ProductsServiceProtocol {
    
    private let requestManager: RequestManagerProtocol
    
    public static let shared: ProductsServiceProtocol = ProductService(requestManager: RequestManager.shared)
    
    // We can also inject requestManager for testing purposes.
    init(requestManager: RequestManagerProtocol) {
        self.requestManager = requestManager
    }
    
    func getProductsCategories(completionHandler: @escaping CategoriesCompletionHandler) {
        self.requestManager.performRequestWith(url: ProductsEndpoint.categories.path, httpMethod: .get) { (result: Result<ProductsCategories, RequestError>) in
            // Taking Data to main thread so we can update UI.
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    func getProducts(page: Int, completionHandler: @escaping ProductsCompletionHandler) {
        self.requestManager.performRequestWith(url: ProductsEndpoint.products(page).path, httpMethod: .get) { (result: Result<Products, RequestError>) in
            // Taking Data to main thread so we can update UI.
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
}
