//
//  ProductsViewModel.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import Foundation

/*
 Since We are calling two api calls I used DispatchGroup to make sure both api are called and the data is ready. 
 */

final class ProductsViewModel {
    
    var productsService: ProductsServiceProtocol
    
    init(productsService: ProductsServiceProtocol) {
        self.productsService = productsService
    }
    
    public var isFinished = false
    private var currentPage = 0
    
    var loading: ((Bool) -> Void)?
    
    var products: (([Product]) -> Void)?
    var categories: (([String]) -> Void)?
    var errorHandler: ((String) -> Void)?
    
    private var allProducts: [Product] = []
    
    private var productsCache: Products?
    private var categoriesCache: ProductsCategories?
    
    var appliedFilters: [String] = []
    
    var dispatchApiGroup: DispatchGroup = DispatchGroup()
    
    func getProducts() {
        
        productsService.getProducts(page: currentPage) { [weak self] result in
            guard let self = self else { return }
            if self.productsCache == nil {
                self.dispatchApiGroup.leave()
            }
            switch result {
            case .success(let products):
                self.currentPage += 1
                if products.next == nil {
                    self.isFinished = true
                }
                if let products = products.result {
                    self.allProducts.append(contentsOf: products)
                }
                self.productsCache = products
                let filteredProducts = self.applyFilters(on: products.result ?? [], filters: self.appliedFilters)
                self.products?(filteredProducts)
            case .failure(let error):
                self.errorHandler?(error.localizedDescription)
            }
        }
    }
    /// Responsible for apply filter on amount of products ( used for pagination )
    private func applyFilters(on products: [Product], filters: [String]) -> [Product] {
        // If the filters is empty, so no need for another checks just return appliedFilters
        if filters.isEmpty {
            self.appliedFilters = []
            return products
        }
        self.appliedFilters = filters
        // Check if product's category is in filters.
        return products
            .filter {filters.contains($0.category ?? "")}
    }
    
    public func applyFilters(filters: [String]) -> [Product] {
        // If the filters is empty, so no need for another checks just return appliedFilters
        if filters.isEmpty {
            self.appliedFilters = []
            return allProducts
        }
        self.appliedFilters = filters
        // Check if product's category is in filters.
        return allProducts
            .filter {filters.contains($0.category ?? "")}
    }
    
    public func remove(categoryFromFilter category: String) {
        self.appliedFilters.removeAll {$0 == category}
    }
    
    public func add(categoryToFilter category: String) {
        self.appliedFilters.append(category)
    }
    
    private func getCategories() {
        
        productsService.getProductsCategories { [weak self] result in
            guard let self = self else { return }
            if self.categoriesCache == nil {
                self.dispatchApiGroup.leave()
            }
            switch result {
            case .success(let categories):
                self.categoriesCache = categories
            case .failure(let error):
                self.errorHandler?(error.localizedDescription)
            }
        }
    }
    
    func getCatalog() {
        dispatchApiGroup.enter()
        getProducts()
        
        dispatchApiGroup.enter()
        getCategories()
        
        self.loading?(true)
        // Both Apis are called and has response so show data to user
        dispatchApiGroup.notify(queue: .main) { [weak self] in
            guard let self = self else { return }
            self.loading?(false)
            
            if let categories = self.categoriesCache?.categories {
                self.categories?(categories)
            }
            if let products = self.productsCache?.result {
                self.products?(products)
            }
        }
    }
}
