//
//  ProductCoordinator.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/9/21.
//

import UIKit

class ProductCoordinator: Coordinator {
    
    var rootViewController: UIViewController?
    
    weak var parentCoordinator: Coordinator?
    
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start(animated: Bool) {
        let categoriesViewController = ProductsViewController.instantiate(coordinator: self)
        navigationController.pushViewController(categoriesViewController, animated: animated)
    }
    
    deinit {
        print("Removed \(self) from memory")
    }
}
