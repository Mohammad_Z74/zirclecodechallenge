//
//  LocalizedStrings.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/9/21.
//

import Foundation

// We can also using swiftgen for generating string files
enum LocalizedStrings: String {
    
    case toProducts = "to_Products"
    case changeLanguage
    case controllFlowNotelabel
    
    var value: String {
        return localized(key: self.rawValue)
    }
    
    private func localized(key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
}
