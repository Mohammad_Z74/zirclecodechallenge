//
//  HTTPMethod.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
}
