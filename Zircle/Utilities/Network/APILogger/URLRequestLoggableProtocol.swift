//
//  URLRequestLoggableProtocol.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import Foundation

protocol URLRequestLoggableProtocol {
    func logResponse(_ response: HTTPURLResponse?, data: Data?, error: Error?, HTTPMethod: String?)
}
