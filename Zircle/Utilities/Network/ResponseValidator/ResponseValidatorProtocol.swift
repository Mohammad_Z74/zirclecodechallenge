//
//  ResponseValidatorProtocol.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import Foundation

protocol ResponseValidatorProtocol {
    func validation<T: Codable>(response: HTTPURLResponse?, data: Data?) -> Result<T, RequestError>
}
