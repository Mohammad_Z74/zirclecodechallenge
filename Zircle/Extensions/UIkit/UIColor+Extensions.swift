//
//  UIColor+Extensions.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/9/21.
//

import UIKit

extension UIColor {
    
    private enum CustomColor: String {
        
        case zalandoOrange
        
        var color: UIColor {
            guard let color = UIColor(named: rawValue) else {
                assertionFailure("Color missing from asset catalogue")
                return .systemOrange
            }
            return color
        }
    }
    
    static var zalandoOrange: UIColor = {
        CustomColor.zalandoOrange.color
    }()
}
