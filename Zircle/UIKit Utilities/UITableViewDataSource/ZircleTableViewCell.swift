//
//  ZircleTableViewCell.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import UIKit

public protocol ZircleTableViewCell: UITableViewCell {
    
    associatedtype CellViewModel
    
    func configureCellWith(_ item: CellViewModel)
    
}
