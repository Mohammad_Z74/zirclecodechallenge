//
//  ZircleCollectionViewCell.swift
//  Zircle
//
//  Created by Mohammad Zakizadeh on 1/10/21.
//

import UIKit

protocol ZircleCollectionViewCell: UICollectionViewCell {
    associatedtype CellViewModel
    func configureCellWith(_ item: CellViewModel)
}
